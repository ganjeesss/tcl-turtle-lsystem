# tcl turtle lsystem

## turtle.tcl

turtle.tcl is a tcl library for doing turtle graphics. e.g.

    :::text
    source turtle.tcl
    
    new_turtle sarah
    
    move sarah 10
    turn sarah 90
    move sarah 10
    turn sarah -45
    move sarah 20
    
    write_turtle_png sarah "example.png" 640 480
    
output is via gnuplot. relevant turtle related procs defined by turtle.tcl are:

    :::text
    new_turtle turtle 
    # creates a new turtle called <turtle> in the current namespace
    
    turn turtle angle 
    # turns the turtle called <turtle> by <angle> degrees
    
    move turtle distance 
    # moves the turtle called <turtle> by <distance>
    
    get_turtle_data turtle 
    # returns a string with the gnuplot vector data of <turtle>'s path until now
    
    write_turtle_png turtle filename width height
    # plots <turtle>'s path until now to a png file <filename> with size <width> x <height>
    
in addition a turtle can be interacted with directly using tcl's built in dict commands since a turtle object is simply a dict with these keys:

* xpos
* ypos
* angle
* colour
* data
    
e.g.

    :::text
    new_turtle sarah
    
    move sarah 10
    dict set sarah colour #ffffff
    move sarah 10
    dict set sarah colour #000000
    move sarah 10
    
    # etc...
    
## lsystem.tcl

lsystem.tcl provides two small functions for rendering l-systems with turtle graphics:

    :::text
    iterate rules state
    # replaces each character in <state> according to the rules in <rules> 
    # (rules is a dict mapping char:replacement)
    # or leaves it the same if it does not exist as a key in <rules>
    # output is a new string containing the new state one iteration after <state>
    
    execute rules state
    # for each character in <state>, if it exists as a <key> in <rules>
    # the corresponding value from <rules> will be evaluated as tcl code in the caller's namespace
    
the examples in ./examples show how these can be used together with functions from ./turtle.tcl to plot l-systems.
    
    
    
    