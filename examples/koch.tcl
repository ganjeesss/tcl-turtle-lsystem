#!/usr/bin/tclsh

source ../turtle.tcl
source ../lsystem.tcl

dict set replacement_rules "F" "F+F-F-F+F"
dict set drawing_rules "F" { move gary 10 }
dict set drawing_rules "-" { turn gary -90 }
dict set drawing_rules "+" { turn gary 90 }

set state "F"

for {set i 0} {$i < 4} {incr i} {
    set state [iterate replacement_rules $state]
}

new_turtle gary
execute drawing_rules $state
write_turtle_png gary "koch.png" 640 480