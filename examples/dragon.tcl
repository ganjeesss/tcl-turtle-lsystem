#!/usr/bin/tclsh

source ../turtle.tcl
source ../lsystem.tcl

dict set replacement_rules "X" "X+YF"
dict set replacement_rules "Y" "FX-Y"

dict set drawing_rules "F" { move gary 10 }
dict set drawing_rules "-" { turn gary 270 }
dict set drawing_rules "+" { turn gary 90 }

set state "FX"

for {set i 0} {$i < 12} {incr i} {
    set state [iterate replacement_rules $state]
}

new_turtle gary
execute drawing_rules $state
write_turtle_png gary "dragon.png" 640 480