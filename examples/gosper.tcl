#!/usr/bin/tclsh

source ../turtle.tcl
source ../lsystem.tcl

dict set replacement_rules "A" "A-B--B+A++AA+B-"
dict set replacement_rules "B" "+A-BB--B-A++A+B"

dict set drawing_rules "A" { move gary 10 }
dict set drawing_rules "B" { move gary 10 }
dict set drawing_rules "-" { turn gary 300 }
dict set drawing_rules "+" { turn gary 60 }

set state "A"

for {set i 0} {$i < 5} {incr i} {
    set state [iterate replacement_rules $state]
}

new_turtle gary
execute drawing_rules $state
write_turtle_png gary "gosper.png" 800 600