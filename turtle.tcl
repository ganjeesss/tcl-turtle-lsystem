#!/usr/bin/tclsh

# tcl gnuplot turtle

# write a file
proc write {data filename} {

    set dfile [open $filename w]
    puts $dfile $data
    close $dfile
    
}

# write a temp file and execute it with gnuplot
proc plot {commands} {

    set cfile [open "TEMP.GNUPLOT" w]
    puts $cfile $commands
    close $cfile
    
    exec -ignorestderr gnuplot TEMP.GNUPLOT
    file delete TEMP.GNUPLOT
    
}

# degrees to radians
proc degs_to_rads {degrees} {

    return [expr {$degrees * 0.017453292519943295769237}]
    
}

# create a turtle
proc new_turtle {name} {

    upvar $name turtle
    
    set turtle [dict create]
    dict set turtle xpos 0
    dict set turtle ypos 0
    dict set turtle angle 0
    dict set turtle colour "00 00 00"
    dict set turtle data ""
    
}

# turn a turtle
proc turn {dict dangle} {

    upvar $dict turtle
    
    set angle [expr {[dict get $turtle angle] + $dangle}]

    while {$angle < 0} { 
            set angle [expr {$angle + 360}]
    }
    
    while {$angle > 360} {
            set angle [expr {$angle - 360}]
    }
    
    dict set turtle angle $angle 
    
}

# move a turtle
proc move {dict length} {

    upvar $dict turtle
    
    set data [dict get $turtle data]
    set x1 [dict get $turtle xpos]
    set y1 [dict get $turtle ypos]
    set colour [dict get $turtle colour]
    set angle [dict get $turtle angle]
    
    set angle [degs_to_rads $angle]
    set dx [expr {sin($angle) * $length}]
    set dy [expr {cos($angle) * $length}]
    
    dict set turtle data "$data\n$x1 $y1 $dx $dy $colour"
    dict set turtle xpos [expr {$x1 + $dx}]
    dict set turtle ypos [expr {$y1 + $dy}]
    
}

# access a turtle's internal data
proc get_turtle_data {dict} {

    upvar $dict turtle
    return [dict get $turtle data]
    
}

# plot a turtle to png file with sane defaults
proc write_turtle_png {dict filename width height} {

    upvar $dict turtle
    write [get_turtle_data turtle] TEMP.DAT
    
    set data {
        set terminal png background rgbcolor "#ffffff" size __SIZE__
        set output "__FILENAME__"
        set size ratio -1
        unset border; unset tics; unset key
        plot "TEMP.DAT" with vectors nohead lc rgb variable lw 1
    }
    
    regsub {__SIZE__} $data "${width},${height}" data
    regsub {__FILENAME__} $data $filename data
    
    plot $data
    file delete TEMP.DAT
    
}
